# mercaYA!   

pagina de de supermercado donde puedes encontrar frutas verduras y varios productos para tu hogar.

## hecho `por`:
```
(c) Johan sebastian echeverry (c) Lucas felipe calvo
```

trabajo hecho con fines educativos para la materia programación 4 de la universidad de Manizales.
## "Nota"
este proyecto se realizo a bases de templates codigos bootstrap e imagenes de diferentes fuentes.

## Links
[bootstrap templates](https://cssauthor.com/top-free-grocery-store-bootstrap-templates/)

[Grocery store](https://templatebazaar.in/bootstrap-theme/grocery-bootstrap-template)

[imagenes](https://www.google.com/search?q=frutas&tbm=isch&ved=2ahUKEwjx35a_lr33AhUGBt8KHfYADJ4Q2-cCegQIABAA&oq=fru&gs_lcp=CgNpbWcQARgAMgcIABCxAxBDMgQIABBDMgcIABCxAxBDMgQIABBDMgcIABCxAxBDMgcIABCxAxBDMgcIABCxAxBDMgQIABBDMgcIABCxAxBDMgcIABCxAxBDUKUDWIIiYJ8yaABwAHgBgAGGAYgB-QmSAQQwLjEwmAEAoAEBqgELZ3dzLXdpei1pbWewAQDAAQE&sclient=img&ei=z-VtYrG8L4aM_Ab2gbDwCQ&bih=679&biw=767&rlz=1C1CHBF_esCO891CO891#imgrc=3N4GWjALsVpa1M&imgdii=WDiuRbVKnGjeXM)

[Material de apoyo de Prog4](https://classroom.google.com/c/NDU0NTAyOTA3NjEw)
